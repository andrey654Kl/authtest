<?php

namespace components;

use Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Класс для связи с базой данных
 *
 * @author kas
 */
class DbQuery {

    const HOST = "localhost";
    const LOGIN = "root";
    const PASS = "123";
    const DB = "backet";

    /**
     * Подключение к базе
     * @return type
     * @throws Exception
     */
    static function getConnect() {
        $con = mysqli_connect(self::HOST, self::LOGIN, self::PASS, self::DB);
        $con->set_charset('utf8');
        if ($con->connect_errno) {
            throw new Exception("Не удалось подключиться к базе данных:" . $con->connect_error);
        }
        return $con;
    }

    /**
     * Функция для выборки значений из базы
     * @param type $query
     * @return boolean
     * @throws Exception
     */
    static function select($query) {
        $con = self::getConnect();
        $selectArray = [];
        $result = $con->query($query, MYSQLI_USE_RESULT);
        if (!$result) {
            throw new Exception('Неверный запрос к базе');
        }
        while ($row = mysqli_fetch_assoc($result)) {
            $selectArray[] = $row;
        }
        $result->free();
        $con->close();
        if (empty($selectArray)) {
            return false;
        } elseif (count($selectArray) == 1) {
            return $selectArray[0];
        } else {
            return $selectArray;
        }
    }

    /**
     * Функция для добавления значений в базу
     * @param type $query
     * @return boolean
     * @throws Exception
     */
    static function insert($query) {
        $con = self::getConnect();
        $result = $con->query($query);
        if (!$result) {
            throw new Exception('Неверный запрос к базе');
        }
        $con->close();
        return $result;
    }

}
