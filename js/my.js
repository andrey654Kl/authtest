function clearFields() {
    $(".errors_text").html("");
    $(".field_input").removeClass("field_success field_error");
}

function addErrorToField(key, error) {
    $(".errors_text").append("<li>" + error + "</li>");
    $('#' + key + '-field').parent().addClass('field_error');
}
function submitAjaxForm(url, data) {
    var key;
    $.ajax({
        url: url,
        type: 'post',
        dataType: "json",
        data: data,
        success: function (data) {
            if (data.success) {
                document.location.href = data.location;
            } else {
                for (key in data.errors) {
                    addErrorToField(key, data.errors[key]);
                }
            }
        }
    });
}