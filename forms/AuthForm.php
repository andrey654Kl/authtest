<?php

namespace forms;

//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Форма для аутенфикации пользователя
 *
 * @author kas
 */
class AuthForm extends BaseForm {

    public $login; // Логин
    public $password; // Пароль
    public $remember = false;  // Аутенфикация пользователя на длительное время
    private $_user;

    function setData($data) {
        if (isset($data['login'])) {
            $this->login = $data['login'];
        }
        if (isset($data['password'])) {
            $this->password = $data['password'];
        }
        if (isset($data['remember'])) {
            $this->remember = true;
        }
    }

    function validate() {
        $return = [];
        $errors = [];
        if (!$this->login) {
            $errors['login'][] = 'Заполните логин';
        } else {
            $lengthLogin = mb_strlen($this->login, 'utf-8');
            if ($lengthLogin < 3 || $lengthLogin > 20) {
                $errors['login'][] = 'Длина логина должна составлять от 3 до 20 символов';
            }
        }
        if (!$this->password) {
            $errors['password'][] = 'Заполните пароль';
        } else {
            $lengthPass = mb_strlen($this->password, 'utf-8');
            if ($lengthPass < 3 || $lengthPass > 20) {
                $errors['password'][] = 'Длина пароля должна составлять от 3 до 20 символов';
            }
        }
        if (empty($errors)) {
            if (!$this->checkExistUser()) {
                $errors['login'][] = 'Неверный логин или пароль';
            }
        }
        if (empty($errors)) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
            $return['errors'] = $errors;
        }
        return $return;
    }

    function handler() {
        $validate = $this->validate();
        if ($validate['success']) {
            \models\User::login($this->_user, ($this->remember ? (3600 * 24 * 30) : 0));
            return true;
        }
        return $validate;
    }

    /**
     * Проверка существования пользователя
     * @return boolean
     */
    function checkExistUser() {
        if ($this->_user) {
            return true;
        }
        $data = \models\User::getUserByLogin($this->login);
        if (!$data) {
            return false;
        } else {
            if ($data['password'] == \models\User::encryptPass($this->password)) {
                $this->_user = $data;
                return true;
            } else {
                return false;
            }
        }
    }

}
