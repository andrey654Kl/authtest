<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace forms;

use models\User;

/**
 * Description of RegForm
 *
 * @author kas
 */
class RegForm extends BaseForm {

    public $login;
    public $password;
    public $name;
    public $about;

    function setData($data) {
        if (isset($data['login'])) {
            $this->login = $data['login'];
        }
        if (isset($data['password'])) {
            $this->password = $data['password'];
        }
        if (isset($data['name'])) {
            $this->name = $data['name'];
        }
        if (isset($data['about'])) {
            $this->about = $data['about'];
        }
    }

    function validate() {
        $return = [];
        $errors = [];
        if (!$this->login) {
            $errors['login'][] = 'Заполните логин';
        } else {
            $lengthLogin = mb_strlen($this->login, 'utf-8');
            if ($lengthLogin < 3 || $lengthLogin > 20) {
                $errors['login'][] = 'Длина логина должна составлять от 3 до 20 символов';
            }
        }
        if (!$this->password) {
            $errors['password'][] = 'Заполните пароль';
        } else {
            $lengthPass = mb_strlen($this->password, 'utf-8');
            if ($lengthPass < 3 || $lengthPass > 20) {
                $errors['password'][] = 'Длина пароля должна составлять от 3 до 20 символов';
            }
        }
        if ($this->name) {
            $lengthName = mb_strlen($this->name, 'utf-8');
            if ($lengthName < 5 || $lengthName > 50) {
                $errors['password'][] = 'Длина ФИО должна составлять от 5 до 50 символов';
            }
        }
        if (empty($errors)) {
            if ($this->checkExistUser()) {
                $errors['login'][] = 'Данный логин уже занят';
            }
        }
        if (!$this->checkValidIP()) {
            $errors['ip'][] = 'Вы недавно регистрировались с данного ip адреса';
        }
        if (empty($errors)) {
            $return['success'] = true;
        } else {
            $return['success'] = false;
            $return['errors'] = $errors;
        }
        return $return;
    }

    function handler() {
        $validate = $this->validate();
        if ($validate['success']) {
            $user = User::addUser([
                        'login' => $this->login,
                        'password' => \models\User::encryptPass($this->password),
                        'name' => $this->name,
                        'about' => $this->about,
                        'ip' => $_SERVER['REMOTE_ADDR']
            ]);
            User::login($user, (3600 * 24 * 30));
            return true;
        }
        return $validate;
    }

    /**
     * Проверка существования пользователя
     * @return boolean
     */
    function checkExistUser() {
        $result = User::getUserByLogin($this->login);
        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Проверка на разрешение регистрации пользователя с данного ip адресса
     * @return boolean
     */
    function checkValidIP() {
        $timeToIP = 3600 * 24; //В течении суток
        $currentIP = $_SERVER['REMOTE_ADDR']; // Получение текущего ip адреса
        $user = User::getUserByIP($currentIP); // Поиск пользователя в базе с данным ip
        if (!$user) {
            return true;
        }
        if (strtotime($user['regDate'] < time() - $timeToIP)) { // Если прошло необходимое время с момента последней регистрации
            return true;
        } else {
            return false;
        }
    }

}
