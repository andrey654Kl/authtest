<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace forms;

/**
 * Description of BaseForm
 *
 * @author kas
 */
abstract class BaseForm {

    //put your code here
    abstract function setData($data); // Установка свойств формы из переданных данных

    abstract function validate(); // проверка правильности заполненности полей

    abstract function handler(); // Обработка формы
}
