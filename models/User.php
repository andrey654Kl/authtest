<?php

namespace models;

use components\DbQuery;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Класс для работы с пользователем
 *
 * @author kas
 */
class User {

    const TABLE_NAME = 'user'; // Название таблицы в базе данных
    const COOKIE_LOGIN = 'login';
    const COOKIE_PASS = 'password';

    //put your code here
    /**
     * Поиск пользователя по имен
     * @param type $login
     * @return boolean
     */
    static function getUserByLogin($login) {
        $data = DbQuery::select("SELECT id,login,password,name FROM " . self::TABLE_NAME . " WHERE login = '" . $login . "' limit 1");
        if ($data) {
            return $data;
        } else {
            return false;
        }
    }

    /**
     * Поиск пользователя по идентификатору
     * @param type $id
     * @return boolean
     */
    static function getUserByID($id) {
        $data = DbQuery::select("SELECT `id`,`login`,`password`,`name` FROM " . self::TABLE_NAME . " WHERE id = '" . (int) $id . "' limit 1");
        if ($data) {
            return $data;
        } else {
            return false;
        }
    }

    /**
     * Поиск пользователя по ip
     * @param type $ip
     * @return boolean
     */
    static function getUserByIP($ip) {
        $data = DbQuery::select("SELECT id,regDate FROM " . self::TABLE_NAME . " WHERE `ip` = '" . $ip . "' limit 1");
        if ($data) {
            return $data;
        } else {
            return false;
        }
    }

    /**
     * Добавление и добавленного пользователя
     * @param array $data
     * @return type
     */
    static function addUser($data) {
        $curentDate = date('Y-m-d H:i:s');
        $data['regDate'] = $curentDate;
        $keys = implode('`,`', array_keys($data));
        $values = implode("','", array_values($data));
        $sql = "INSERT INTO " . self::TABLE_NAME . " (`" . $keys . "`) values ('" . $values . "')";
        DbQuery::insert($sql);
        $user = self::getUserByLogin($data['login']);
        return $user;
    }

    /**
     * Шифрование пароля
     * @param type $pass
     * @return type
     */
    static function encryptPass($pass) {
        return md5($pass);
    }

    /**
     * Аутенфикация пользователя
     * @param type $user
     * @param type $time Время авторизации пользователя
     */
    static function login($user, $time = 0) {
        if ($time) {
            setcookie(self::COOKIE_LOGIN, $user['login'], time() + $time);
            setcookie(self::COOKIE_PASS, self::encryptPass($user['login'] . $user['password']), time() + $time);
        }
        $_SESSION['userID'] = $user['id'];
        $_SESSION['userName'] = $user['name'];
    }

    /**
     * Проверка авторизованности пользователя
     * @return boolean
     */
    static function isLogin() {
        if (isset($_SESSION['userID'])) {
            return true;
        } else { //если сессии нет, то проверим существование cookie. Если они существуют, то проверим их валидность по БД 	
            if (isset($_COOKIE[self::COOKIE_LOGIN]) && isset($_COOKIE[self::COOKIE_PASS])) { //если куки существуют. 		
                $user = self::getUserByLogin($_COOKIE[self::COOKIE_LOGIN]);
                if (self::encryptPass($user['login'] . $user['password']) == $_COOKIE[self::COOKIE_PASS]) { //если логин и пароль нашлись в БД 			
                    self::login($user);
                    return true;
                } else { // 			
                    self::deleteUserCookie();
                    return false;
                }
            } else { //если куки не существуют 		
                return false;
            }
        }
    }

    /**
     * Удаление данных в куках об пользователи
     */
    static function deleteUserCookie() {
        setcookie(self::COOKIE_LOGIN, "", time() - 3600, '/');
        setcookie(self::COOKIE_PASS, "", time() - 3600, '/');
    }

}
