<?php
include_once 'layout/header.php';
if (!models\User::isLogin()):
    header("location:login");// Редирект пользователя если он не авторизован
else:
    ?>
    <div class="container">	
        <div id="welcome">
            <h2>Добро пожаловать, <span><?php echo $_SESSION['userName']; ?>! </span></h2>
            <p><a href="logout">Выйти</a> из системы</p>
        </div>
    </div>
<?php endif; ?>