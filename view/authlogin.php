
<?php
if (models\User::isLogin()) {
    header("Location: main");// Редирект если пользователь уже авторизован
}
include_once 'layout/header.php';
?>
<div class="container">
    Поля отмеченные * обязательны к заполнению
    <div id="auth_form">
        <form method="post" id="auth_form_id">
            <ul class="errors_text">
            </ul>
            <div class="field_input">
                <label for="login-field">Логин*</label><br>
                <input type="text" name="AuthForm[login]" id="login-field" class="formstr">
            </div>
            <div class="field_input">
                <label for="password-field">Пароль*</label><br>
                <input type="password" name="AuthForm[password]" id="password-field" class="formstr" >
            </div>
            <div class="field_input">
                <label for="remeber-field">Запомнить меня</label>
                <input type="checkbox" name="AuthForm[remember]" id="remeber-field" >
            </div>
            <p class="regtext">Еще не зарегистрированы?<a href="reg">Регистрация</a>!</p>
            <input type="submit" value="Войти" name="auth" class="submit">
        </form>
    </div>
</div>
<script>
    $("#auth_form_id").submit(function () {
        clearFields();
        submitAjaxForm('auth', $(this).serialize());
        return false;
    });
</script>