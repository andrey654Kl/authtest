<?php
if (models\User::isLogin()) {
    header("Location: main");// редирект пользователя если он уже авторизован
}
include_once 'layout/header.php';
?>
<div class="container">
    Поля отмеченные * обязательны к заполнению
    <div id="reg_form">
        <form method="post" id="reg_form_id">
            <ul class="errors_text">
            </ul>
            <div class="field_input">
                <label for="login-field">Логин*</label><br>
                <input type="text" name="RegForm[login]" id="login-field" class="formstr">
            </div>
            <div class="field_input field_success">
                <label for="password-field">Пароль*</label><br>
                <input type="password" name="RegForm[password]" id="password-field" class="formstr" >
            </div>
            <div class="field_input">
                <label for="name-field">ФИО</label><br>
                <input type="text" name="RegForm[name]" id="name-field" class="formstr">
            </div>
            <div class="field_input">
                <label for="about-field">О себе</label><br>
                <textarea name="RegForm[about]" id="about-field" class="formarea">
                </textarea>
            </div>
            <p class="regtext">Уже зарегистрированы? <a href="login">Заходите на сайт</a>!</p>
            <input type="submit" value="Зарегистрироваться" name="reg" class="submit" >
        </form>
    </div>
</div>
<script>
    $("#reg_form_id").submit(function () {
        clearFields();
        submitAjaxForm('regist', $(this).serialize());
        return false;
    });

</script>