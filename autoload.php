<?php

spl_autoload_register('autoload');

function autoload($className) {
    $path = str_replace(["\\"], [DIRECTORY_SEPARATOR], $className);
    $fileName = $path . '.php';
    include_once $fileName;
}

?>