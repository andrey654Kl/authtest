<?php

use forms\RegForm;

if (!isset($_POST['RegForm'])) {
    throw new \Exception('Неверное обращение');
}
$model = new RegForm();
$data = $_POST['RegForm'];
$model->setData($data);
$reg = $model->handler();
if ($reg === true) {
    echo json_encode(['success' => true, 'location' => 'main']);
} else {
    echo json_encode($reg);
}
