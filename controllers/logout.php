<?php

if ($_SESSION['userID']) {
    unset($_SESSION['userID']);
    setcookie("login", "");
    setcookie("password", "");
    session_destroy();
    header("location:login");
}
?>
