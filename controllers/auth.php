<?php
use forms\AuthForm;

if (!isset($_POST['AuthForm'])) {
    throw new \Exception('Неверное обращение');
}
$model = new AuthForm();
$data = $_POST['AuthForm'];
$model->setData($data);
$login = $model->handler();
if ($login === true) {
    echo json_encode(['success' => true,'location'=>'main']);
} else {
    echo json_encode($login);
}

