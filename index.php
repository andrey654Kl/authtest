<?php

include_once 'autoload.php';
session_start();
$page = 'view/main'; // Отображение по умолчанию
if (isset($_GET['page'])) {
    switch ($_GET['page']) { // В зависимости от переданного параметра отображаем страницу
        case 'reg': $page = 'view/reg';
            break;
        case 'logout': $page = 'controllers/logout';
            break;
        case 'main': $page = 'view/main';
            break;
        case 'login': $page = 'view/authlogin';
            break;
        case 'auth': $page = 'controllers/auth';
            break;
        case 'regist': $page = 'controllers/registration';
            break;
    }
}

include_once($page . '.php');

